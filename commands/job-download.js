const JobFiles = require('./job-files');
const DataProcess = require('dataprocess').DataProcess;
const Promise = require('bluebird');
const FS = require('fs-extra');
const Path = require('path');
const Log = (...data) => console.error('=>', ...data);

function DownloadFile(file) {
    const {id, relativePath} = file;
    const URL = `https://platform.rescale.com/api/v2/files/${id}/contents/`;
    const RESCALE_API_TOKEN = process.env['RESCALE_API_TOKEN'];
    const Fetch = require('node-fetch');

    Log(`Downloading: (${id})`, relativePath, '...');
    return Fetch(URL, {
        method: 'GET',
        headers: {
            'Authorization': `Token ${RESCALE_API_TOKEN}`
        }
    })
        .then(res => res.buffer())
        .then(contents => FS.writeFile(relativePath, contents))
}

module.exports = (id = 'prTuXb') => DataProcess('download-files')
    .tap('create-directory', ({relativePath}) => FS.ensureDir(Path.dirname(relativePath)))
    .map('download-file', DownloadFile)
    .run(JobFiles(id), {concurrency: 10})
