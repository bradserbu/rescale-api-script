module.exports = (name, command, nodes , cores , coreType, inputFiles) => {
    name = name ||  'CLI Job';
    command = command || './Allrun';
    inputFiles = inputFiles ||  'SdFEpc';
    nodes = nodes || 1;
    cores = cores || 1;
    coreType = coreType || 'hpc-3';

    const FS = require('fs-extra');
    const Fetch = require('node-fetch');
    const job = ({
        "name": name,
        "jobanalyses": [
            {
                "useMpi": false,
                "command": command,
                // "analysis": {
                    // "code": "openfoam",
                    // "version": "2.3.0-openmpi"

                // },
                "analysis": {
                    "code": "openfoam_plus",
                    "version": "v1706+-intelmpi",
                },
                "hardware": {
                    "coresPerSlot": cores,
                    "slots": nodes,
                    "coreType": coreType
                },
                "inputFiles": [
                    {
                        "id": inputFiles
                    }
                ]
            }
        ]
    });

    const URL = `https://platform.rescale.com/api/v2/jobs/`;
    const RESCALE_API_TOKEN = process.env['RESCALE_API_TOKEN'];

    return Fetch(URL, {
        method: 'post',
        headers: {
            'Authorization': `Token ${RESCALE_API_TOKEN}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(job),
    })
        .then(res => res.json());
}
