module.exports = (filepath = 'input.zip') => {
    const RESCALE_API_TOKEN = process.env['RESCALE_API_TOKEN'];
    const URL = 'https://platform.rescale.com/api/v2/files/contents/';
    const Fetch = require('node-fetch');
    const FormData = require('form-data');
    const FS = require('fs');
    const Path = require('path');

    const form = new FormData();
    form.append('file', FS.readFileSync(Path.resolve(filepath)), {
        filename: filepath,
        filepath: filepath
    });

    return Fetch(URL, {
        method: 'POST',
        headers: {
            'Authorization': `Token ${RESCALE_API_TOKEN}`,
            // 'Content-Type': 'Content-Type:multipart/form-data'
            ...form.getHeaders()
        },
        body: form
    })
        .then(res => res.json());
}
