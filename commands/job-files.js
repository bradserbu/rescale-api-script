module.exports = (id = 'prTuXb') => {
    const $ = require('highland');
    let URL = `https://platform.rescale.com/api/v2/jobs/${id}/files/`;
    const RESCALE_API_TOKEN = process.env['RESCALE_API_TOKEN'];
    const Fetch = require('node-fetch');
    const DataProcess = require('dataprocess').DataProcess;
    const Promise = require('bluebird');

    let page = 0;
    const ListFiles = () => $(function ($push, $next) {
        // do something async when we read from the Stream
        // fs.readFile(filename, function (err, data) {
        //     push(err, data);
        //     push(null, _.nil);
        // });
        Fetch(page > 0 ? `${URL}?page=${page}` : URL, {
            method: 'GET',
            headers: {
                'Authorization': `Token ${RESCALE_API_TOKEN}`
            }
        })
            .then(res => res.json())
            .then(({next, results}) => {
                results.forEach(result => $push(null, result));
                page++;
                next ? $next() : $push(null, $.nil);
            });
    });

    return DataProcess('list-files')
        .run(ListFiles());
};
