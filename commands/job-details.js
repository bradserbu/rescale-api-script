module.exports = (id = 'prTuXb') => {
    const URL = `https://platform.rescale.com/api/v2/jobs/${id}/`;
    const RESCALE_API_TOKEN = process.env['RESCALE_API_TOKEN'];
    const Fetch = require('node-fetch');

    return Fetch(URL, {
        method: 'GET',
        headers: {
            'Authorization': `Token ${RESCALE_API_TOKEN}`
        }
    })
        .then(res => res.json())
        // .then(({results}) => results);
};
