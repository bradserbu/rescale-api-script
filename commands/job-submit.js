module.exports = (id = 'QTVia') => {
    const URL = `https://platform.rescale.com/api/v2/jobs/${id}/submit/`;
    const RESCALE_API_TOKEN = process.env['RESCALE_API_TOKEN'];
    const Fetch = require('node-fetch');
    const Log = (...data) => console.error('=>', ...data);

    Log('Submitting job...');
    return Fetch(URL, {
        method: 'POST',
        headers: {
            'Authorization': `Token ${RESCALE_API_TOKEN}`
        }
    })
        .then(async res => {
            if (res.ok) {
                // res.status >= 200 && res.status < 300
                // return res.toString();
                // return res.body;
                // console.log(res.ok);
                // console.log(res.status);
                // console.log(res.statusText);
                // console.log(res.headers.raw());
                // console.log(res.headers.get('content-type'));
                // return {id, status: 'Submitted'};
                return id;
            } else {
                // throw Error(res.statusText);
                return res.json();
            }
        });
};
