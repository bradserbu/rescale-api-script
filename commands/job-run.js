const RESCALE_API_TOKEN = process.env['RESCALE_API_TOKEN'];
const Log = (...data) => console.error('=>', ...data);
const RescaleApi = (path, options) => {
    const Fetch = require('node-fetch');
    const URL = `https://platform.rescale.com/api/v2/${path}`;
    options = options || {};
    options.method = options.method || 'GET';
    options.headers = options.headers || {};
    options.headers = Object.assign(options.headers, {
        Authorization: `Token ${RESCALE_API_TOKEN}`,
    });
    return Fetch(URL, options);
};
const Zip = (path = process.cwd()) => new Promise((resolve, reject) => {
    const Path = require('path');
    const OS = require('os');
    const FS = require('fs');
    const archiver = require('archiver');
    const dir = Path.join(OS.tmpdir(), 'input.zip');
    const output = FS.createWriteStream(dir);
    const archive = archiver('zip');

    // output.on('close',  () => resolve({size: archive.pointer()}));
    output.on('close', () => resolve(dir));
    archive.on('error', reject);
    archive.pipe(output);

    // append files from a sub-directory and naming it `new-subdir` within the archive (see docs for more options):
    Log('Zipping job files...');
    archive.directory(path, false);
    archive.finalize();

    return archive;
});
const Upload = (filepath) => {
    const Path = require('path');
    const FS = require('fs');
    const FormData = require('form-data');
    const form = new FormData();
    Log('Uploading job files...');
    form.append('file', FS.readFileSync(filepath), {
        filename: Path.basename(filepath),
        filepath: Path.basename(filepath)
    });

    return RescaleApi('files/contents/', {
        method: 'POST',
        headers: form.getHeaders(),
        body: form
    })
        .then(res => res.json())
};
const CreateJob = require('./job-create');
const SubmitJob = require('./job-submit');
module.exports = (name, command, nodes, cores, coreType) => Zip()
    .then(Upload)
    .then(({id: inputFiles}) => CreateJob(name, command, nodes, cores, coreType, inputFiles))
    .then(({id}) => SubmitJob(id))
