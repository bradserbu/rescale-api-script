const _ = require('lodash');
const Promise = require('bluebird');
const Program = require('nodus-framework').program;
const JobStatus = require('./job-status');
const Log = (...data) => console.error('=>', ...data);

let DONE = false;
Program.on('shutdown', () => DONE = true);

module.exports = (id, status = 'Completed', interval = 1000) => {

    Log(`Waiting for job to reach '${status}' status...`);
    const CheckStatus = () => JobStatus(id)
        .then(statuses => _.filter(statuses, s => s.status == status))
        .then(matches => matches.length > 0)
        .then(matches => (!matches && !DONE)
            ? Promise.delay(interval).then(() => CheckStatus())
            : {matches});

    return CheckStatus();
};
