module.exports = (path = 'airfoil2D') => new Promise((resolve, reject) => {
    const FS = require('fs');
    const archiver = require('archiver');
    const output = FS.createWriteStream('input.zip');
    const archive = archiver('zip');

    output.on('close',  () => resolve({size: archive.pointer()}));
    archive.on('error', reject);
    archive.pipe(output);

    // append files from a sub-directory and naming it `new-subdir` within the archive (see docs for more options):
    archive.directory(path, false);
    archive.finalize();
});
