#!/usr/bin/env bash

## Store Current Working Directory
CWD=$PWD

## Directory of 'run.sh' script
DIRNAME="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

## Rescale Script
RESCALE="${DIRNAME}/../rescale"

## Extract 'airfoil2D.zip' job files
unzip airfoil2D

## Change to Job Directory
cd airfoil2D

## Run Job -> Store JOBID
JOBID=$( $RESCALE job-run )

# Display Job Id
echo "-------------------------"
echo "- Job: $JOBID"
echo "-------------------------"

## Wait for Job to Complete
$RESCALE job-wait ${JOBID}

## Create Output Directory
mkdir ${CWD}/${JOBID}.output

## Change Directory to Output Directory
cd ${CWD}/${JOBID}.output

## Download Output Files
$RESCALE job-download ${JOBID}

## Change back to working directory
cd $CWD

## Display mesage
echo "Job run successfully."
echo ${CWD}/${JOBID}.output
