# Rescale API Script
The Rescale API script makes it easy to run jobs in the cloud using the Rescale API.

## API Credentials
Use the 'RESCALE_API_TOKEN' environment variable to set your API credentials for connecting to the Rescale Platform
```
export RESCALE_API_TOKEN='a665ac2f73a34f28fa39b8c9eb1c62cb243eXXXX'
``` 

## Adding to Path
You should add the 'rescale' command script to your path so that it can be used from any directory containing a job.

```
export PATH=$PWD:$PATH
```

## Submitting a Job
The following command is used to submit a job to the rescale platform.

```
rescale job-run    # returns <job-id>
```

It should be run from the directory containing your job files.

The command will automatically create an `input.zip` file containing your job submission script and input files.

## Job Details
Use the `job-details` command to see the details of the job.

```
rescale job-details <job-id> ---table
```

## Job Status
Use the `job-status` command to monitor the status of the job.

```
rescale job-status <job-id> --table
```

## Job Events
The `job-events` command can be used to see a more detailed log of events.
```
rescale job-events <job-id> --table
```

## Waiting Status
The `job-wait` command can be used to wait for a job to reach the completed state.
```
rescale job-wait <job-id>
```

## Output Files
After the job has finished executing the `job-files` command can be used to see a list of output files produced by the job.
```
rescale job-files <job-id>
```

## Downloading
Use the `job-download` command to download all the job files to the current directory.
```
rescale job-download <job-id>
```   

## Example
The `examples` directory contains a complete end-to-end example provided with a `run.sh` script demonstrating how the above commands can be used together to automate the submission of any job using Rescale.

```
cd examples; ./run.sh
```
